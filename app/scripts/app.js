'use strict';

/**
 * @ngdoc overview
 * @name tp3GliApp
 * @description
 * # tp3GliApp
 *
 * Main module of the application.
 */
angular
  .module('tp3GliApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
