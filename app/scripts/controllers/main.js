'use strict';

/**
 * @ngdoc function
 * @name tp3GliApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tp3GliApp
 */
angular.module('tp3GliApp')
  .controller('MainCtrl', function ($scope,$http) {

    $scope.userAll = function () {
        $http.get("/user")
          .then(function (response) {
            $scope.userResp = response.data;
          }
        )
    };

    $scope.userName = "";

    $scope.userGet = function () {
      if ($scope.userName) {
        $http.get("/user/" + $scope.userName)
            .then(function (response) {
              $scope.userResp = response.data;
            }
        )
      }
    };

    $scope.userAdd = function () {
      if ($scope.userName) {
          $http.post("user/" + $scope.userName,"")
              .then(function (response) {
                  $scope.userResp = response.statusText;
              },
              function() {
                  $scope.userResp = "Bad request";
              }
          )
      }
    };

    $scope.userProjectName = "";
    $scope.userAddProject = function () {
      if ($scope.userName && $scope.userProjectName) {
          $http.post("/user/" + $scope.userName + "/AddProject=" + $scope.userProjectName,"")
              .then(function (response) {
                  $scope.userResp = response.statusText;
              },
              function() {
                  $scope.userResp = "Bad request";
              }
          )
      }
    };

    $scope.userRemoveProject = function () {
      if ($scope.userName && $scope.userProjectName) {
          $http.post("/user/" + $scope.userName + "/RemoveProject=" + $scope.userProjectName,"")
              .then(function (response) {
                  $scope.userResp = response.statusText;
              },
              function() {
                  $scope.userResp = "Bad request";
              }
          )
      }
    };


    $scope.projectAll = function () {
        $http.get("/project")
            .then(function (response) {
                $scope.projectResp = response.data;
            }
        )
    };

    $scope.projectName = "";

    $scope.projectGet = function () {
        if ($scope.projectName) {
            $http.get("/project/" + $scope.projectName)
                .then(function (response) {
                    $scope.projectResp = response.data;
                }
            )
      }
    };

    $scope.projectAdd = function () {
        if ($scope.projectName) {
            $http.post("project/" + $scope.projectName,"")
                .then(function (response) {
                    $scope.projectResp = response.statusText;
                },
                function() {
                    $scope.projectResp = "Bad request";
                }
            )
        }
    };

    $scope.projectRemove = function () {
        if ($scope.projectName) {
            $http.delete("project/" + $scope.projectName,"")
                .then(function (response) {
                    $scope.projectResp = response.statusText;
                },
                function() {
                    $scope.projectResp = "Bad request";
                }
            )
      }
    };

        $scope.backlogAll = function () {
            $http.get("/backlog")
                .then(function (response) {
                    $scope.backlogResp = response.data;
                }
            )
        };

        $scope.backlogName = "";

        $scope.backlogGet = function () {
            if ($scope.backlogName) {
                $http.get("/backlog/" + $scope.backlogName)
                    .then(function (response) {
                        $scope.backlogResp = response.data;
                    }
                )
            }
        };
  });
